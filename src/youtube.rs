use crate::error::Error;
use cached::Cached;
use serde::Deserialize;
use serenity::static_assertions::_core::fmt::Display;
use std::str::FromStr;
use url::Url;

const API_URL: &str = "https://www.googleapis.com/youtube/v3/";

pub fn get_id_from_url(url: &Url) -> String {
    let query = url
        .query_pairs()
        .find(|(k, _v)| k == "v")
        .map(|(_k, v)| v.to_string());

    let path = String::from(url.path()).replace("/", "");

    query.unwrap_or_else(|| path)
}

pub fn id_to_url(id: &str) -> String {
    format!("https://www.youtube.com/watch?v={}", id)
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Snippet {
    pub title: String,
    pub channel_title: String,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct SearchItem {
    pub kind: String,
    pub id: ApiID,
    pub snippet: Snippet,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct QueryItem {
    pub kind: String,
    pub snippet: Snippet,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ApiID {
    kind: String,
    video_id: Option<String>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct ApiResponse<I> {
    pub items: Vec<I>,
}

pub async fn search(query: &str) -> Result<Option<String>, Error> {
    let key = std::env::var("GOOGLE_KEY").expect("GOOGLE_KEY wasn't set");
    let mut url = Url::from_str(API_URL).unwrap().join("search").unwrap();
    url.set_query(Some(&format!(
        "part=snippet&key={}&q={}&type=video",
        key, query,
    )));

    tracing::info!("Sending Request to {}", url.as_str());

    let response = reqwest::get(url)
        .await?
        .json::<ApiResponse<SearchItem>>()
        .await?;

    Ok(response
        .items
        .into_iter()
        .filter_map(|item| item.id.video_id.clone())
        .next())
}

pub async fn get_video_info_cached(
    id: impl ToString,
    cache: &mut cached::stores::SizedCache<String, Snippet>,
) -> Result<Snippet, Error> {
    let result = if let Some(response) = cache.cache_get(&id.to_string()) {
        Ok(response.clone())
    } else {
        let response = get_video_info(&id.to_string()).await?;
        cache.cache_set(id.to_string(), response.clone());
        Ok(response)
    };
    result
}

pub async fn get_video_info(id: &str) -> Result<Snippet, Error> {
    let key = std::env::var("GOOGLE_KEY").expect("GOOGLE_KEY wasn't set");
    let mut url = Url::from_str(API_URL).unwrap().join("videos")?;
    url.set_query(Some(format!("part=snippet&key={}&id={}", key, id).as_str()));

    tracing::info!("Sending request to {}", url.as_str());

    let response = reqwest::get(url.as_str())
        .await?
        .json::<ApiResponse<QueryItem>>()
        .await?;

    response
        .items
        .into_iter()
        .find(|item| item.kind == "youtube#video")
        .map(|item| item.snippet)
        .ok_or_else(|| Error::internal("Youtube api didn't respond with a snippet"))
}

pub fn id_to_link(link: impl Display) -> String {
    format!("https://youtu.be/{}", link)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_resource_from_url_removes_slashes() {
        assert_eq!(
            get_id_from_url(&Url::parse("https://www.youtube.com/watch?v=dQw4w9WgXcQ").unwrap()),
            "dQw4w9WgXcQ"
        );

        assert_eq!(
            get_id_from_url(&Url::parse("https://youtu.be/dQw4w9WgXcQ").unwrap()),
            "dQw4w9WgXcQ"
        );
    }

    #[tokio::test]
    async fn can_get_name_of_not_rick_roll() {
        let _ = dotenv::dotenv();
        get_video_info("dQw4w9WgXcQ").await.unwrap();
    }

    #[tokio::test]
    async fn can_search() {
        let _ = dotenv::dotenv();
        search("ACDC").await.unwrap().unwrap();
    }
}
