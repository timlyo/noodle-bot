use songbird::input::Metadata;

pub fn format_rounded_duration(duration: chrono::Duration) -> String {
    let second = duration.num_seconds() % 60;
    let minute = duration.num_minutes() % 60;
    let hour = duration.num_hours() % 24;
    let day = duration.num_days();

    match (day, hour, minute, second) {
        (0, 0, 0, second) => format!("{} seconds", second),
        (0, 0, minute, 0) => format!("{} minutes", minute),
        (0, 0, minute, second) => format!("{} minutes, {} seconds", minute, second),
        (0, hour, minute, _) => format!("{} hours, {} minutes", hour, minute),
        (day, _, _, _) => format!("{} days", day),
    }
}

pub fn title_author_from_metadata(metadata: &Metadata) -> String {
    match (&metadata.title, &metadata.artist) {
        (None, None) => String::from("Untitled"),
        (Some(title), None) => title.clone(),
        (None, Some(artist)) => format!("Unknown Title - {}", artist),
        (Some(title), Some(artist)) => format!("{} - {}", title, artist),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::Duration;

    #[cfg(test)]
    mod test_format_time {
        use super::*;

        #[test]
        fn time_with_seconds_only_has_seconds() {
            for x in 1..60 {
                let duration = Duration::seconds(x);
                assert_eq!(format_rounded_duration(duration), format!("{} seconds", x));
            }
        }

        #[test]
        fn minute_no_seconds() {
            for minute in 1..60 {
                let duration = Duration::minutes(minute);
                assert_eq!(
                    format_rounded_duration(duration),
                    format!("{} minutes", minute)
                )
            }
        }

        #[test]
        fn minute_time_returns_seconds_and_minute() {
            for minute in 1..60 {
                for second in 1..60 {
                    let duration = Duration::minutes(minute) + Duration::seconds(second);
                    assert_eq!(
                        format_rounded_duration(duration),
                        format!("{} minutes, {} seconds", minute, second)
                    );
                }
            }
        }
    }
}
