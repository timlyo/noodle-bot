use std::sync::Arc;

use serenity::model::id::{ChannelId, GuildId};
use serenity::model::prelude::{Guild, Message, User};
use serenity::prelude::Context;
use songbird::Songbird;

use crate::error::Error;
use crate::events;
use serenity::http::Http;

pub async fn message_channel(
    ctx: impl AsRef<Http>,
    channel: &ChannelId,
    msg: impl ToString,
) -> Result<Message, serenity::Error> {
    channel
        .send_message(ctx, |m| m.content(msg.to_string()))
        .await
}

pub async fn get_users_current_channel(guild: &Guild, user: &User) -> Option<ChannelId> {
    guild
        .voice_states
        .get(&user.id)
        .and_then(|vs| vs.channel_id)
}

pub async fn is_song_playing(ctx: &Context, msg: &Message) -> Result<bool, Error> {
    let manager = get_voice(ctx).await;
    let call = manager
        .get(msg.guild_id.unwrap())
        .ok_or_else(|| Error::internal("Couldn't get manager"))?;
    let call = call.lock().await;

    Ok(!call.queue().is_empty())
}

pub fn package_messages(messages: impl Iterator<Item = String>, limit: usize) -> Vec<String> {
    let mut result = Vec::new();
    let mut buffer = Vec::new();
    let mut buffer_len = 0;

    for message in messages {
        assert!(message.len() <= limit);

        if message.len() + buffer_len <= limit {
            buffer_len += message.len();
            buffer.push(message);
        } else {
            result.push(buffer.join(""));
            buffer_len = message.len();
            buffer = Vec::from([message]);
        }
    }

    result.push(buffer.join(""));

    result
}

pub async fn get_voice(ctx: &Context) -> Arc<Songbird> {
    songbird::get(ctx)
        .await
        .expect("Songbird wasn't registered")
}

pub async fn send_embedded_list(
    ctx: &Context,
    channel: &ChannelId,
    list: Vec<(String, String)>,
    embed: bool,
) -> Result<Message, serenity::Error> {
    channel
        .send_message(ctx, |m| {
            m.embed(|e| {
                for (name, value) in list {
                    e.field(name, value, embed);
                }
                e
            })
        })
        .await
}

pub async fn join_channel_of_author(ctx: &Context, msg: &Message) -> Result<(), Error> {
    let guild = msg
        .guild(ctx)
        .await
        .ok_or_else(|| Error::internal("Failed to get msg's guild"))?;

    let user_channel = guild
        .voice_states
        .get(&msg.author.id)
        .and_then(|voice_state| voice_state.channel_id);

    let current_bot_channel = guild
        .voice_states
        .get(&ctx.cache.current_user().await.id)
        .and_then(|voice_state| voice_state.channel_id);

    if user_channel == current_bot_channel {
        tracing::info!("Already in user's channel");
        return Ok(());
    }

    if let Some(channel) = user_channel {
        let voice_manager = get_voice(ctx).await;

        tracing::info!("Joining {}:{}", guild.name, channel);
        let join_result = voice_manager.join(guild.id, channel).await;

        return match join_result {
            (call, Ok(())) => {
                events::register_events_for_guild(call).await;
                tracing::info!("Successfully joined channel");
                Ok(())
            }
            (_call, Err(join_result)) => {
                tracing::info!("Channel join failed: {:?}", join_result);
                Err(Error::internal("Failed to join channel"))
            }
        };
    } else {
        msg.reply(ctx, "User didn't have a channel").await?;
    }

    tracing::info!("Joined successfully");
    Ok(())
}

pub async fn leave_channel(
    ctx: &Context,
    guild: &GuildId,
) -> Result<(), songbird::error::JoinError> {
    get_voice(ctx).await.leave(*guild).await
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn package_messages_combines_strings() {
        let messages = (0..10)
            .map(|x| format!("{}_________", x))
            .collect::<Vec<_>>();
        let total = messages.join("");

        let len = 20;

        let res = package_messages(messages.into_iter(), len);

        assert_eq!(total, res.join(""));

        assert_eq!(res.len(), 5);

        for item in res {
            assert!(item.len() <= len);
        }
    }
}
