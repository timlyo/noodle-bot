use crate::database::models::{Song, SongRequest};
use crate::discord;
use chrono::Utc;
use serenity::model::id::{ChannelId, GuildId, UserId};
use songbird::typemap::TypeMapKey;
use sqlx::postgres::PgQueryResult;
use sqlx::PgPool;

pub fn get_error_code(err: &sqlx::Error) -> Option<PostgresCodes> {
    err.as_database_error()
        .and_then(|e| e.code())
        .map(|code| match code.as_ref() {
            "23505" => PostgresCodes::UniqueViolation,
            _ => PostgresCodes::Unknown,
        })
}

pub enum PostgresCodes {
    UniqueViolation,
    Unknown,
}

pub mod models {
    use super::*;
    use crate::error::Error;
    use serenity::model::prelude::Message;
    use serenity::prelude::Context;

    #[derive(Debug)]
    pub struct SongRequest {
        pub yt_video: String,
        pub count: Option<i64>,
    }

    #[derive(Debug)]
    pub struct Song<'a> {
        pub requester: UserId,
        pub guild: GuildId,
        pub request_channel: ChannelId,
        pub audio_channel: Option<ChannelId>,
        pub id: &'a str,
        pub time_requested: chrono::DateTime<Utc>,
    }

    impl<'a> Song<'a> {
        pub async fn from_ctx(
            ctx: &'a Context,
            msg: &'a Message,
            id: &'a str,
        ) -> Result<Song<'a>, Error> {
            let guild = msg
                .guild(ctx)
                .await
                .ok_or_else(|| Error::internal("Tried to get guild outside of guild"))?;

            Ok(Song {
                requester: msg.author.id,
                guild: guild.id,
                request_channel: msg.channel_id,
                audio_channel: discord::get_users_current_channel(&guild, &msg.author).await,
                id,
                time_requested: Utc::now(),
            })
        }
    }
}

pub struct DbConnectionKey {}

impl TypeMapKey for DbConnectionKey {
    type Value = PgPool;
}

pub async fn get_connection() -> Result<PgPool, sqlx::Error> {
    let _ = dotenv::dotenv();

    let url = std::env::var("DATABASE_URL").expect("DATABASE_URL environment variable wasn't set");
    sqlx::PgPool::connect(&url).await
}

pub async fn record_song_request(
    pool: &PgPool,
    song: Song<'_>,
) -> Result<PgQueryResult, sqlx::Error> {
    sqlx::query!(
        r#"INSERT INTO songs
            (requester, guild, request_channel, audio_channel, yt_video, time_requested)
        VALUES
            ($1, $2, $3, $4, $5, $6)
        "#,
        song.requester.0.to_string(),
        song.guild.0.to_string(),
        song.request_channel.0.to_string(),
        song.audio_channel.map(|ac| ac.to_string()),
        song.id,
        song.time_requested
    )
    .execute(pool)
    .await
}

pub async fn get_top_played(
    pool: &PgPool,
    guild: &GuildId,
) -> Result<Vec<SongRequest>, sqlx::Error> {
    sqlx::query_as!(
        SongRequest,
        r#"SELECT count(*) as count, yt_video
        FROM public.songs
        WHERE guild = $1
        GROUP BY yt_video
        ORDER BY count DESC, yt_video
        LIMIT 10
        "#,
        guild.0.to_string()
    )
    .fetch_all(pool)
    .await
}

pub async fn get_top_but_not_recently_played(
    pool: &PgPool,
    guild: &GuildId,
) -> Result<Vec<SongRequest>, sqlx::Error> {
    sqlx::query_as!(
        SongRequest,
        r#"SELECT count(*) as count, yt_video
        FROM public.songs
        WHERE yt_video NOT IN 
            (SELECT yt_video FROM songs ORDER BY time_requested LIMIT 10)
            AND guild = $1
        GROUP BY yt_video
        ORDER BY count DESC, yt_video
        LIMIT 10
        "#,
        guild.0.to_string()
    )
    .fetch_all(pool)
    .await
}

pub async fn count_songs(pool: &PgPool, guild: &GuildId) -> Result<(i64, i64), sqlx::Error> {
    let total = sqlx::query!(
        r#"SELECT COUNT(*) FROM songs WHERE guild = $1"#,
        guild.0.to_string()
    )
    .fetch_one(pool)
    .await?;

    let unique = sqlx::query!(
        r#"SELECT COUNT(DISTINCT yt_video) FROM songs WHERE guild = $1"#,
        guild.0.to_string()
    )
    .fetch_one(pool)
    .await?;

    Ok((unique.count.unwrap(), total.count.unwrap()))
}
