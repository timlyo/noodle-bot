use async_trait::async_trait;
use serenity::prelude::*;
use songbird::EventHandler;
use songbird::{Call, Event, EventContext, TrackEvent};
use std::sync::Arc;

pub async fn register_events_for_guild(manager: Arc<Mutex<Call>>) {
    tracing::info!("Registering events");
    let mut handle = manager.lock().await;

    handle.add_global_event(
        Event::Track(TrackEvent::End),
        TrackEndEvent {
            manager: manager.clone(),
        },
    );
}

pub struct TrackEndEvent {
    manager: Arc<Mutex<Call>>,
}

#[async_trait]
impl EventHandler for TrackEndEvent {
    async fn act(&self, _ctx: &EventContext<'_>) -> Option<Event> {
        let mut manager = self.manager.lock().await;

        if manager.queue().is_empty() {
            tracing::info!("Queue empty, leaving channel");
            if let Err(e) = manager.leave().await {
                tracing::info!("{:?}", e);
            }
        }

        None
    }
}
