#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("Serenity {0}")]
    Serenity(#[from] serenity::prelude::SerenityError),
    #[error("Songbird join {0}")]
    SongbirdJoin(#[from] songbird::error::JoinError),
    #[error("Track Error {0}")]
    SongbirdTrackError(#[from] songbird::error::TrackError),
    #[error("Error {0}")]
    Internal(String),
    #[error("{0}")]
    UserFacing(String),
    #[error("Error parsing url {0}")]
    UrlParseError(#[from] url::ParseError),
    #[error("HTTP Error {0}")]
    HttpError(#[from] reqwest::Error),
    #[error("Database Error {0}")]
    DatabaseError(#[from] sqlx::Error),
}

impl Error {
    pub fn internal(msg: impl ToString) -> Error {
        Error::Internal(msg.to_string())
    }

    pub fn user_facing(msg: impl ToString) -> Error {
        Error::UserFacing(msg.to_string())
    }
}
