use crate::{discord, util};
use serenity::{
    framework::{
        standard::macros::command,
        standard::{Args, CommandResult},
    },
    model::prelude::*,
    prelude::*,
};
use std::sync::Arc;
use std::time::Instant;
use tokio::time::Duration;

#[command]
#[only_in(guild)]
pub async fn time(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let duration = humantime::parse_duration(args.rest());
    let start_time = Instant::now();

    let (mut message, duration) = match duration {
        Ok(time) => (
            msg.reply(
                ctx,
                format!(
                    "Set timer for {time}, remainder: {time}",
                    time = humantime::format_duration(time)
                ),
            )
            .await?,
            time,
        ),
        Err(_) => {
            msg.reply(ctx, "Couldn't parse time").await?;
            return Ok(());
        }
    };

    let msg_content = message
        .content
        .split(":")
        .into_iter()
        .next()
        .unwrap_or_else(|| "Set timer, remainder:")
        .to_string();

    let ctx = Arc::new(ctx.clone());
    let author = msg.author.clone();

    tokio::spawn(async move {
        loop {
            let elapsed = Instant::now() - start_time;
            let remaining = duration.checked_sub(elapsed);

            if remaining.is_none() {
                let _ = message
                    .edit(&*ctx, |e| e.content(format!("{} none", msg_content)))
                    .await;
                let _ = discord::message_channel(
                    &*ctx,
                    &message.channel_id,
                    format!(
                        "{} Timer for {} complete!",
                        author,
                        humantime::format_duration(duration)
                    ),
                )
                .await;
                break;
            } else {
                // Remaining is only None if elapsed is greater than duration. None case handled above
                let remaining = remaining.unwrap();

                let _ = message
                    .edit(&*ctx, |e| {
                        e.content(format!(
                            "{} {}",
                            msg_content,
                            util::format_rounded_duration(
                                chrono::Duration::from_std(remaining.clone()).unwrap()
                            )
                        ))
                    })
                    .await;
            }

            if let Some(remaining) = remaining {
                if remaining > Duration::from_secs(60) {
                    tokio::time::sleep(Duration::from_secs(15)).await;
                } else if remaining > Duration::from_secs(10) {
                    tokio::time::sleep(Duration::from_secs(5)).await;
                } else {
                    tokio::time::sleep(Duration::from_secs(1)).await;
                }
            } else {
                tokio::time::sleep(Duration::from_secs(10)).await;
            }
        }
    });

    Ok(())
}
