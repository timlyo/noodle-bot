use serenity::{
    framework::standard::{macros::command, CommandResult},
    model::prelude::{GuildId, Message},
    prelude::Context,
};
use sqlx::PgPool;

use crate::IdCacheKey;

use crate::{database, discord, youtube, Error};

#[command]
#[only_in(guild)]
#[description = "Print instance stats"]
pub async fn stats(ctx: &Context, msg: &Message) -> CommandResult {
    let db = {
        let data = ctx.data.write().await;
        data.get::<database::DbConnectionKey>()
            .cloned()
            .expect("DB Connection wasn't setup")
    };

    tracing::info!("Stats");

    let (unique_songs, total_songs) = database::count_songs(&db, &msg.guild_id.unwrap()).await?;

    discord::message_channel(
        ctx,
        &msg.channel_id,
        format!(
            "Played {} total songs, {} unique",
            total_songs, unique_songs
        ),
    )
    .await?;

    let top_10 = get_top(&db, ctx, &msg.guild_id.unwrap()).await?;

    if top_10.is_empty() {
        discord::message_channel(ctx, &msg.channel_id, "Nothing has been requested yet").await?;
    } else {
        let list = top_10
            .into_iter()
            .map(|(count, title, id)| {
                (
                    format_playcount(count),
                    format!("[{}]({})", title, youtube::id_to_link(&id)),
                )
            })
            .collect::<Vec<_>>();
        discord::send_embedded_list(ctx, &msg.channel_id, list, false).await?;
    }

    Ok(())
}

fn format_playcount(count: i64) -> String {
    match count {
        0 => String::from("No Plays"),
        1 => String::from("Played once"),
        n => format!("Played {} times", n),
    }
}

pub async fn get_top(
    db: &PgPool,
    ctx: &Context,
    guild: &GuildId,
) -> Result<Vec<(i64, String, String)>, Error> {
    let top = database::get_top_played(db, guild).await?;
    let mut data = ctx.data.write().await;
    let id_cache = data.get_mut::<IdCacheKey>().expect("ID cache wasn't setup");

    let mut result = Vec::new();
    for song in top {
        let title = youtube::get_video_info_cached(song.yt_video.clone(), id_cache)
            .await
            .map(|s| s.title)
            .unwrap_or(song.yt_video.clone());
        result.push((song.count.unwrap(), title, song.yt_video))
    }

    Ok(result)
}
