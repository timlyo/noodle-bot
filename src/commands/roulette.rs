use crate::commands::play_now;
use crate::{discord, get_noodles};
use serenity::model::prelude::Message;
use serenity::{
    framework::standard::{macros::command, CommandResult},
    prelude::*,
};
use std::time::Duration;
use tokio::time::sleep;

fn get_spin() -> u8 {
    (rand::random::<u8>() % 6) + 1
}

async fn run_spinner(
    ctx: &Context,
    message: &mut Message,
    quick: bool,
) -> Result<u8, SerenityError> {
    if quick {
        return Ok(get_spin());
    }

    let mut num = 0;

    for _ in 0..6 {
        num = get_spin();
        message
            .edit(ctx, |m| {
                m.content(format!("Spinning the barrel now! {}", num))
            })
            .await?;
        sleep(Duration::from_millis(500)).await;
    }

    Ok(num)
}

#[command]
#[description = "Roll a die and kick dexter if you get lucky, or he does, or something ..."]
pub async fn roulette(ctx: &Context, msg: &Message) -> CommandResult {
    let quick = msg.content.contains("quick") && msg.author.id == crate::TIMLYO;
    let always = msg.content.contains("always") && msg.author.id == crate::TIMLYO;
    let no_kick = msg.content.contains("no-kick");

    let sleep_time = if quick {
        std::time::Duration::from_millis(0)
    } else {
        std::time::Duration::from_millis(750)
    };

    if let Some(guild_id) = msg.guild_id {
        msg.channel_id
            .say(ctx, "Welcome to Dexter Roulette!")
            .await?;
        sleep(sleep_time).await;
        let mut message = msg.channel_id.say(ctx, "Spinning the barrel now!").await?;

        sleep(sleep_time).await;

        let num = run_spinner(ctx, &mut message, quick).await?;

        if always || num == 6 {
            msg.channel_id
                .send_message(ctx, |m| m.content("HE'S OUT OF HERE!"))
                .await?;

            let noodles = get_noodles()
                .await
                .map_err(|e| format!("Failed to get Noodles.wav: {:?}", e))?;

            discord::join_channel_of_author(ctx, &msg).await?;
            play_now(ctx, &guild_id, noodles, 2.5)
                .await
                .map_err(|e| format!("{:?}", e))?;

            if !discord::is_song_playing(ctx, msg).await? {
                discord::leave_channel(ctx, &guild_id).await?;
            }

            if !no_kick {
                guild_id
                    .edit_member(ctx, crate::DEXTER, |m| m.disconnect_member())
                    .await?;
            }
        } else {
            msg.channel_id.say(ctx, "Got lucky this time ...").await?;
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;
    use std::iter::FromIterator;

    use super::*;

    #[test]
    fn test_get_spin() {
        let spins = (0..1000).map(|_| get_spin()).collect::<HashSet<_>>();

        assert_eq!(spins, HashSet::from_iter(Vec::from([1, 2, 3, 4, 5, 6])))
    }
}
