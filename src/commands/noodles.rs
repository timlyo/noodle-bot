use crate::{get_noodles, Error};
use serenity::{
    framework::standard::{macros::command, CommandResult},
    model::prelude::*,
    prelude::*,
};
use std::time::Duration;

#[command]
#[description = "noodles"]
pub async fn noodles(ctx: &Context, msg: &Message) -> CommandResult {
    let guild_id = msg
        .guild_id
        .ok_or_else(|| Error::internal("Failed to get guild id"))?;

    crate::discord::join_channel_of_author(ctx, msg).await?;

    for x in 0..10 {
        let noodles = get_noodles()
            .await
            .map_err(|e| Error::internal(format!("Failed to get noodles.wav {:?}", e)))?;
        crate::commands::play_now(ctx, &guild_id, noodles, (x as f32) * 0.5).await?;
    }

    tokio::time::sleep(Duration::from_secs(2)).await;

    Ok(())
}
