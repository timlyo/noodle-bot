//! Command for roll tables. Set of items that can be randomly selected between
//!
//! Features: Add, list table, grouping of tables

use crate::commands::util;
use crate::discord;
use crate::error::Error;
use rand::prelude::SliceRandom;
use serenity::framework::standard::Args;
use serenity::{
    framework::standard::macros::{command, group},
    framework::standard::CommandResult,
    model::prelude::Message,
    prelude::Context,
};
use sqlx::PgPool;
use structopt::StructOpt;
use util::CommunicableError;

#[group]
#[commands(list, new, add, roll, remove, restore)]
#[prefixes("rt", "rolltable")]
#[only_in("guilds")]
#[default_command(list)]
#[description = "Set of commands for creating Roll Tables"]
pub struct RollTable;

/// List roll tables or roll table items. If empty everything will be listed
///
/// `!noodles rolltable list` will list everything
///
/// `!noodles rolltable list table names` will list the items in the `names` table
///
/// `!noodles rt list group games` will list the tables in the `games` group
///
/// `!noodles rt list games` will also list the table in the `games` group, unless a `games` table
/// exists, then it will list items in the table.
#[derive(StructOpt, Debug)]
#[structopt(name = "list")]
struct ListOpts {
    /// Put the name of an item here to list it, or specify group or table to be specific
    what: Option<String>,
    /// If what is group or table then specify a name to list it
    name: Option<String>,
}

#[command]
pub async fn list(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let args: ListOpts = match util::parse_args(args) {
        Ok(opts) => opts,
        Err(e) => {
            discord::message_channel(ctx, &msg.channel_id, e.to_string()).await?;
            return Ok(());
        }
    };

    tracing::info!(args = ?args, "Listing roll tables");

    let data = ctx.data.read().await;
    let db = data.get::<database::DbConnectionKey>().unwrap();

    // users can list
    // - All tables in a guild sorted by group (no parameters)
    // - All tables in a group (what: group, value: group_name)
    // - All items in a table (what: table, value: table_name)
    // - Automatic resolve, search tables, then groups (what: name, value: None)

    let res = match (args.what.as_deref(), args.name.as_deref()) {
        (None, None) => Ok(list_actions::list_tables_in_guild(ctx, msg, db).await?),
        (Some("group"), None) => Err(String::from("Group must be specified")),
        (Some("table"), None) => Err(String::from("Table must be specified")),
        (Some("group"), Some(name)) => {
            Ok(list_actions::list_tables_in_group(ctx, msg, db, name).await?)
        }
        (Some("table"), Some(name)) => {
            Ok(list_actions::list_items_in_table(ctx, msg, db, name).await?)
        }
        (Some(name), _) => Ok(list_actions::dynamic(ctx, msg, db, name).await?),
        (None, Some(_)) => {
            tracing::error!("Value was supplied without a what for rt list. Should be impossible");
            Err(String::from("What must be supplied if value is"))
        }
    };

    if let Err(e) = res {
        discord::message_channel(ctx, &msg.channel_id, e).await?;
    }

    Ok(())
}

mod list_actions {
    use super::*;
    use std::collections::HashMap;

    pub async fn list_tables_in_guild(ctx: &Context, msg: &Message, db: &PgPool) -> CommandResult {
        let tables = database::get_tables_for_guild(db, msg.guild_id.unwrap()).await?;

        if tables.is_empty() {
            let message =
                "No tables have been created yet. Add one with `!noodles rolltable new <name>`";
            discord::message_channel(ctx, &msg.channel_id, message).await?;
            return Ok(());
        }

        tracing::info!(tables = ?tables);

        let mut table_by_group = HashMap::new();
        for table in tables {
            let group = table_by_group
                .entry(table.table_group)
                .or_insert_with(|| Vec::new());
            group.push(table.name);
        }

        msg.channel_id
            .send_message(ctx, |e| {
                e.embed(|e| {
                    e.title("Roll Tables");

                    for (group, tables) in table_by_group {
                        if let Some(group) = group {
                            e.field(group, tables.join(", "), false);
                        } else {
                            e.description(tables.join(","));
                        }
                    }

                    e
                })
            })
            .await?;

        Ok(())
    }

    pub async fn list_tables_in_group(
        ctx: &Context,
        msg: &Message,
        db: &PgPool,
        group: &str,
    ) -> CommandResult {
        let tables = database::get_tables_in_group(db, msg.guild_id.unwrap(), group).await?;
        let names = tables.into_iter().map(|t| t.name).collect::<Vec<_>>();

        msg.channel_id
            .send_message(ctx, |e| {
                e.embed(|e| e.title(group).description(names.join(", ")))
            })
            .await?;
        Ok(())
    }

    pub async fn list_items_in_table(
        ctx: &Context,
        msg: &Message,
        db: &PgPool,
        table_name: &str,
    ) -> CommandResult {
        let table = database::get_table_by_name(db, table_name, None, msg.guild_id.unwrap())
            .await?
            .ok_or_else(|| {
                Error::user_facing(format!("Couldn't find a table named {}", table_name))
            })
            .communicate_error(ctx, msg)
            .await?;

        let items = database::get_table_items(db, table.id).await?;

        if items.is_empty() {
            let message = format!(
                "{table} doesn't have any items yet, add some with `!noodles rolltable add {table} \"My thing\"`",
                table = table_name
            );
            discord::message_channel(ctx, &msg.channel_id, message).await?;
        } else {
            let content = items.into_iter().map(|i| i.content).collect::<Vec<_>>();

            msg.channel_id
                .send_message(ctx, |e| {
                    e.embed(|e| e.title(table_name).description(content.join(", ")))
                })
                .await?;
        }

        Ok(())
    }

    pub async fn dynamic(ctx: &Context, msg: &Message, db: &PgPool, name: &str) -> CommandResult {
        if let Some(_table) =
            database::get_table_by_name(db, name, None, msg.guild_id.unwrap()).await?
        {
            list_items_in_table(ctx, msg, db, name).await?;
        } else if database::group_exists(db, name, msg.guild_id.unwrap()).await? {
            list_tables_in_group(ctx, msg, db, name).await?;
        } else {
            discord::message_channel(
                ctx,
                &msg.channel_id,
                format!("Couldn't find a table or a group called {}", name),
            )
            .await?;
        }

        Ok(())
    }
}

#[derive(StructOpt, Debug)]
#[structopt(name = "new")]
struct NewOpts {
    /// Name of the rolltable to be created
    name: String,
    /// Group to put the table into. Optional, only used for organising tables
    group: Option<String>,
    /// Whether the table is weighted or not
    weighted: Option<bool>,
}

#[command]
#[description = "Creates a new roll table, with an optional group `rt new <name> [group]`"]
pub async fn new(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let args: NewOpts = match util::parse_args(args) {
        Ok(opts) => opts,
        Err(e) => {
            discord::message_channel(ctx, &msg.channel_id, e.to_string()).await?;
            return Ok(());
        }
    };

    tracing::info!("Adding new Roll Table {:?}", args);

    let data = ctx.data.read().await;
    let db = data.get::<database::DbConnectionKey>().unwrap();

    // TODO check if table exists and is deleted

    database::insert_new_table(
        db,
        msg.guild_id.unwrap(),
        msg.author.id,
        &args.name,
        args.group.as_deref(),
        args.weighted.unwrap_or(false),
    )
    .await
    .communicate_error(ctx, msg)
    .await?;

    let message = if let Some(group) = args.group {
        format!("Created a new table called {}:{}", group, args.name)
    } else {
        format!("Created a new table called {}", args.name)
    };

    discord::message_channel(ctx, &msg.channel_id, message).await?;

    Ok(())
}

#[derive(StructOpt, Debug)]
#[structopt(name = "add")]
struct AddOpts {
    table: String,
    content: String,
    weight: Option<i16>,
    link: Option<String>,
}

#[command]
#[description = "Add a roll item to a table"]
pub async fn add(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let args: AddOpts = match util::parse_args(args) {
        Ok(opts) => opts,
        Err(e) => {
            discord::message_channel(ctx, &msg.channel_id, e.to_string()).await?;
            return Ok(());
        }
    };

    tracing::info!("Adding new Roll Table Item {:?}", args);

    let data = ctx.data.read().await;
    let db = data.get::<database::DbConnectionKey>().unwrap();

    let table_id = database::insert_new_item(
        db,
        msg.guild_id.unwrap(),
        &args.content,
        &args.table,
        args.link,
        args.weight.unwrap_or(1),
    )
    .await
    .communicate_error(ctx, msg)
    .await?;

    let count = database::count_items_in_table(db, table_id).await?;

    discord::message_channel(
        ctx,
        &msg.channel_id,
        format!("Added to {}, it now has {} items", args.table, count),
    )
    .await?;

    Ok(())
}

#[derive(StructOpt, Debug)]
#[structopt(name = "remove")]
struct RemoveOpts {
    /// Table to remove, or delete an item from
    table: String,
    item: Option<String>,
}

/// If just a table is supplied then flag it as deleted and show how to restore, If a table and item
/// is supplied then delete the item, but show how to restore it
#[command]
#[description = "Remove a roll item from a table, or the table itself"]
pub async fn remove(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let args: RemoveOpts = match util::parse_args(args) {
        Ok(opts) => opts,
        Err(e) => {
            discord::message_channel(ctx, &msg.channel_id, e.to_string()).await?;
            return Ok(());
        }
    };

    tracing::info!("Removing item {:?}", args);

    let data = ctx.data.read().await;
    let db = data.get::<database::DbConnectionKey>().unwrap();

    let table =
        match database::get_table_by_name(db, &args.table, None, msg.guild_id.unwrap()).await? {
            Some(t) => t,
            None => {
                msg.reply(ctx, format!("Couldn't find a table called {}", args.table))
                    .await?;
                return Ok(());
            }
        };

    if let Some(item_content) = args.item.as_deref() {
        remove_item(ctx, msg, db, &table, item_content).await?;
    } else {
        remove_table(ctx, msg, db, &table).await?;
    }

    Ok(())
}

async fn remove_table(
    ctx: &Context,
    msg: &Message,
    db: &PgPool,
    table: &database::RollTable,
) -> CommandResult {
    let res = database::flag_table_deleted(db, table.id).await?;

    if res.rows_affected() == 0 {
        discord::message_channel(ctx, &msg.channel_id, format!("Failed to find roll table"))
            .await?;
        return Err(Box::new(Error::internal(format!(
            "Failed to delete roll table {:?} {:?}",
            table, msg
        ))));
    } else {
        let message = format!(
            "Removed {table_name}. Use `!n rt restore {table_name}` to get it back or \
            `!n rt new {table_name}` to overwrite it forever",
            table_name = table.name
        );
        discord::message_channel(ctx, &msg.channel_id, message).await?;
    }
    Ok(())
}

async fn remove_item(
    ctx: &Context,
    msg: &Message,
    db: &PgPool,
    table: &database::RollTable,
    item_content: &str,
) -> CommandResult {
    let res = database::delete_item(db, item_content, table.id).await?;
    match res.rows_affected() {
        0 => {
            let similar = database::get_similar_items(db, item_content, table.id).await?;
            discord::message_channel(
                ctx,
                &msg.channel_id,
                format!(
                    "Couldn't find an item called {} in table {}",
                    item_content, table.name
                ),
            )
            .await?;
            discord::message_channel(
                ctx,
                &msg.channel_id,
                format!("Did you mean any of these items: {}", similar.join(", ")),
            )
            .await?;
        }
        rows_deleted => {
            let message = format!("Removed {content} from {table_name}. Run `!n rt add {table_name} {content}` to restore it", content=item_content, table_name=table.name);
            discord::message_channel(ctx, &msg.channel_id, message).await?;

            if rows_deleted > 1 {
                tracing::error!(
                    what = "Multiple roll items have been deleted at once",
                    msg = ?msg,
                    deleted_count = rows_deleted
                );
                return Err(Box::new(Error::internal(
                    "Multiple roll items have been deleted at once",
                )));
            }
        }
    }

    Ok(())
}

#[derive(StructOpt, Debug)]
#[structopt(name = "restore")]
struct RestoreOpts {
    /// Table to restore
    table: String,
}

#[command]
#[description = "Restore a deleted roll table"]
pub async fn restore(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let args: RestoreOpts = match util::parse_args(args) {
        Ok(opts) => opts,
        Err(e) => {
            discord::message_channel(ctx, &msg.channel_id, e.to_string()).await?;
            return Ok(());
        }
    };

    tracing::info!(args = ?args, "Restoring");

    let data = ctx.data.read().await;
    let db = data.get::<database::DbConnectionKey>().unwrap();

    let table = database::get_table_by_name(db, &args.table, None, msg.guild_id.unwrap()).await?;

    match table {
        Some(table) => {
            database::restore_table(db, table.id).await?;
            discord::message_channel(ctx, &msg.channel_id, format!("Restored {}", table.name))
                .await?;
        }
        None => {
            let message = format!("Couldn't find a table called {}", args.table);
            discord::message_channel(ctx, &msg.channel_id, message).await?;
        }
    }

    Ok(())
}

#[command]
#[description = "Rename a roll table"]
pub async fn rename(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    Ok(())
}

#[derive(StructOpt, Debug)]
#[structopt(name = "roll")]
struct RollOpts {
    table: String,
    group: Option<String>,
}

#[command]
pub async fn roll(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let args: RollOpts = match util::parse_args(args) {
        Ok(opts) => opts,
        Err(e) => {
            discord::message_channel(ctx, &msg.channel_id, e.to_string()).await?;
            return Ok(());
        }
    };

    let data = ctx.data.read().await;
    let db = data.get::<database::DbConnectionKey>().unwrap();

    let table = database::get_table_by_name(
        db,
        &args.table,
        args.group.as_deref(),
        msg.guild_id.unwrap(),
    )
    .await?;

    let table = match table {
        None => {
            let message = format!("Couldn't find table {}", args.table);
            discord::message_channel(ctx, &msg.channel_id, message).await?;
            return Ok(());
        }
        Some(table) => table,
    };

    let items = database::get_table_items(db, table.id).await?;

    if items.is_empty() {
        discord::message_channel(
            ctx,
            &msg.channel_id,
            format!(
                "{} doesn't have any items uet. Add some with `!noodles rolltable add`",
                table.name
            ),
        )
        .await?;
    } else {
        // Safe unwrap, only `None` if empty
        let choice = items.choose(&mut rand::thread_rng()).unwrap();
        // TODO link rendering
        discord::message_channel(ctx, &msg.channel_id, format!("I choose {}", choice.content))
            .await?;
    }

    Ok(())
}

pub mod database {
    pub use crate::database::{get_error_code, DbConnectionKey, PostgresCodes};
    use crate::error::Error;
    use serenity::model::id::{GuildId, UserId};
    use sqlx::postgres::PgQueryResult;
    use sqlx::{Executor, PgPool, Postgres};
    use uuid::Uuid;

    #[derive(Debug)]
    pub struct RollTable {
        pub id: uuid::Uuid,
        pub guild_id: String,
        pub creator_id: String,
        pub name: String,
        pub table_group: Option<String>,
        pub global_table: bool,
        pub weighted: bool,
        pub deleted: bool,
    }

    #[derive(Debug)]
    pub struct Item {
        pub content: String,
        pub roll_table: uuid::Uuid,
        pub link: Option<String>,
        pub weight: i16,
    }

    pub async fn flag_table_deleted(
        db: &PgPool,
        table: Uuid,
    ) -> Result<PgQueryResult, sqlx::Error> {
        sqlx::query!(
            r#"UPDATE roll_tables SET deleted = true WHERE id = $1"#,
            table
        )
        .execute(db)
        .await
    }

    pub async fn delete_item(
        db: &PgPool,
        content: &str,
        roll_table: Uuid,
    ) -> Result<PgQueryResult, sqlx::Error> {
        sqlx::query!(
            r#"DELETE FROM roll_table_items WHERE content = $1 AND roll_table = $2"#,
            content,
            roll_table
        )
        .execute(db)
        .await
    }

    // Only returns their content
    pub async fn get_similar_items(
        db: &PgPool,
        content: &str,
        roll_table: Uuid,
    ) -> Result<Vec<String>, sqlx::Error> {
        sqlx::query!(
            r#"SELECT content FROM roll_table_items 
            WHERE roll_table = $1 AND
            LEVENSHTEIN("content", $2) < 5
            LIMIT 5"#,
            roll_table,
            content
        )
        .map(|r| r.content)
        .fetch_all(db)
        .await
    }

    pub async fn group_exists(
        db: &PgPool,
        name: &str,
        guild: GuildId,
    ) -> Result<bool, sqlx::Error> {
        sqlx::query!(
            r#"SELECT * FROM roll_tables WHERE table_group = $1 AND guild_id = $2 LIMIT 1"#,
            name,
            guild.0.to_string()
        )
        .fetch_optional(db)
        .await
        .map(|option| option.is_some())
    }

    pub async fn get_tables_for_guild(
        db: &PgPool,
        guild: GuildId,
    ) -> Result<Vec<RollTable>, sqlx::Error> {
        let guild = guild.0.to_string();
        sqlx::query_as!(
            RollTable,
            r#"SELECT *
            FROM roll_tables
            WHERE (guild_id = $1 OR global_table = TRUE) AND deleted = FALSE
            ORDER BY table_group,name
            "#,
            guild
        )
        .fetch_all(db)
        .await
    }

    pub async fn get_tables_in_group(
        db: &PgPool,
        guild: GuildId,
        group: &str,
    ) -> Result<Vec<RollTable>, sqlx::Error> {
        let guild = guild.0.to_string();
        sqlx::query_as!(
            RollTable,
            r#"SELECT *
            FROM roll_tables
            WHERE (guild_id = $1 OR global_table = TRUE) AND deleted = FALSE AND table_group = $2
            ORDER BY table_group,name
            "#,
            guild,
            group
        )
        .fetch_all(db)
        .await
    }

    pub async fn restore_table(db: &PgPool, id: Uuid) -> Result<PgQueryResult, sqlx::Error> {
        sqlx::query!("UPDATE roll_tables SET deleted = FALSE WHERE id = $1", id)
            .execute(db)
            .await
    }

    pub async fn insert_new_table(
        db: &PgPool,
        guild: GuildId,
        creator: UserId,
        name: &str,
        group: Option<&str>,
        weighted: bool,
    ) -> Result<Uuid, Error> {
        // Fully delete a table if it's been flagged deleted before
        if let Some(table) = get_table_by_name(db, name, None, guild).await? {
            if table.deleted {
                sqlx::query!("DELETE FROM roll_tables WHERE id = $1", table.id)
                    .execute(db)
                    .await?;
            }
        }

        sqlx::query!(
            r#"
            INSERT INTO roll_tables (guild_id, creator_id, name, table_group, weighted)
            VALUES ($1, $2, $3, $4, $5)
            RETURNING id
        "#,
            guild.0.to_string(),
            creator.0.to_string(),
            name,
            group,
            weighted
        )
        .fetch_one(db)
        .await
        .map(|r| r.id)
        .map_err(|e| {
            let code = get_error_code(&e);
            match code {
                None | Some(PostgresCodes::Unknown) => Error::DatabaseError(e),
                Some(PostgresCodes::UniqueViolation) => {
                    Error::UserFacing(format!("A table called {} already exists", name))
                }
            }
        })
    }

    pub async fn insert_new_item(
        db: &PgPool,
        guild: GuildId,
        content: &str,
        roll_table: &str,
        link: Option<String>,
        weight: i16,
    ) -> Result<Uuid, Error> {
        // Check table exists and guild has access to it
        let table_id: Uuid = {
            let roll_table_id = sqlx::query!(
                r#"SELECT id FROM roll_tables WHERE name = $1 AND guild_id = $2"#,
                roll_table,
                guild.0.to_string()
            )
            .fetch_optional(db)
            .await?;

            match roll_table_id {
                None => {
                    return Err(Error::user_facing(format!(
                        "Couldn't find a roll table called {} in this guild",
                        roll_table
                    )))
                }
                Some(row) => row.id,
            }
        };

        // Insert
        sqlx::query!(
            r#"
            INSERT INTO roll_table_items (content, roll_table, link, weight)
            VALUES ($1, $2, $3, $4)
            "#,
            content,
            table_id,
            link,
            weight
        )
        .execute(db)
        .await
        .map_err(|e: sqlx::Error| match get_error_code(&e) {
            None | Some(PostgresCodes::Unknown) => Error::DatabaseError(e),
            Some(PostgresCodes::UniqueViolation) => {
                Error::UserFacing(format!("That roll item already exists"))
            }
        })?;

        Ok(table_id)
    }

    pub async fn get_table_by_name<'a, E>(
        db: E,
        name: &str,
        table_group: Option<&str>,
        guild: GuildId,
    ) -> Result<Option<RollTable>, sqlx::Error>
    where
        E: Executor<'a, Database = Postgres>,
    {
        sqlx::query_as!(
            RollTable,
            r#"SELECT * FROM roll_tables 
            WHERE 
                name = $1 AND
                (guild_id = $2 OR global_table)"#,
            name,
            guild.0.to_string()
        )
        .fetch_optional(db)
        .await
    }

    pub async fn count_items_in_table(db: &PgPool, id: Uuid) -> Result<i64, sqlx::Error> {
        sqlx::query!(
            r#"SELECT COUNT(*) FROM roll_table_items WHERE roll_table = $1"#,
            id
        )
        .map(|r| r.count.unwrap())
        .fetch_one(db)
        .await
    }

    pub async fn get_table_items(db: &PgPool, table_id: Uuid) -> Result<Vec<Item>, sqlx::Error> {
        sqlx::query_as!(
            Item,
            r#"SELECT * FROM roll_table_items WHERE roll_table = $1"#,
            table_id
        )
        .fetch_all(db)
        .await
    }
}
