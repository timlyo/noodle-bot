use std::fmt;

use rand::prelude::SliceRandom;
use serenity::framework::standard::ArgError;
use serenity::model::id::GuildId;
use serenity::static_assertions::_core::convert::TryFrom;
use serenity::static_assertions::_core::fmt::Formatter;
use serenity::{
    framework::standard::macros::command,
    framework::standard::{Args, CommandResult},
    model::channel::Message,
    prelude::Context,
};
use songbird::input::Input;
use url::Url;

use crate::{IdCacheKey, StatsdKey};

use crate::commands::music::queue::{clear_queue, print_queue, remove_from_queue};
use crate::database::DbConnectionKey;
use crate::discord::{get_voice, join_channel_of_author};
use crate::util;
use crate::{database, database::models::Song, discord, error::Error, youtube};

async fn write_queue_response(
    ctx: &Context,
    msg: &Message,
    queue_pos: usize,
    song_id: &str,
) -> CommandResult {
    let info = {
        let mut data = ctx.data.write().await;
        let mut cache = data.get_mut::<IdCacheKey>().unwrap();
        youtube::get_video_info_cached(song_id, &mut cache).await?
    };

    if queue_pos == 1 {
        discord::message_channel(ctx, &msg.channel_id, format!("Queued up {}", info.title)).await?;
    } else {
        discord::message_channel(
            ctx,
            &msg.channel_id,
            format!("Queued up {}, it's #{} in the queue", info.title, queue_pos),
        )
        .await?;
    }

    Ok(())
}

async fn sing_good_shit(ctx: &Context, msg: &Message) -> CommandResult {
    let top_10 = {
        let data = ctx.data.read().await;
        let db = data.get::<DbConnectionKey>().unwrap();
        let mut top_10 =
            database::get_top_but_not_recently_played(db, &msg.guild_id.unwrap()).await?;

        top_10.shuffle(&mut rand::thread_rng());

        top_10
    };

    crate::discord::join_channel_of_author(ctx, msg).await?;

    tracing::info!(?top_10, "Singing good shit");

    for song in top_10 {
        let url = youtube::id_to_url(&song.yt_video);
        let source = songbird::ytdl(&url)
            .await
            .map_err(|e| Error::internal(format!("Error playing song {:?}", e)))?;

        let queue_pos = queue_song(ctx, &msg.guild_id.unwrap(), source).await?;
        write_queue_response(ctx, msg, queue_pos, &song.yt_video).await?;
    }

    Ok(())
}

#[command]
#[only_in(guild)]
#[description = "Sing stuff. Pass a youtube link to play that video, write words to search youtube, or just sing to play a selection"]
pub async fn sing(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    ctx.data
        .read()
        .await
        .get::<StatsdKey>()
        .unwrap()
        .incr("command.sing.calls");

    let what = match args.single::<String>() {
        Ok(what) => what,
        Err(ArgError::Eos) => return sing_good_shit(ctx, msg).await,
        _ => {
            discord::message_channel(ctx, &msg.channel_id, "Can't sing that").await?;
            return Ok(());
        }
    };

    tracing::info!("Singing {}", what);

    let guild_id = msg
        .guild_id
        .ok_or_else(|| Error::internal("Tried to get guild id when it was None"))?;

    if what.starts_with("http") {
        if let Ok(source) = songbird::ytdl(&what).await {
            let song_id = youtube::get_id_from_url(&Url::parse(&what)?);
            let song = Song::from_ctx(ctx, msg, &song_id).await?;

            crate::discord::join_channel_of_author(ctx, msg).await?;
            let queue_pos = queue_song(ctx, &guild_id, source).await?;
            write_queue_response(ctx, msg, queue_pos, &song_id).await?;

            if let Some(db) = ctx.data.read().await.get::<database::DbConnectionKey>() {
                database::record_song_request(db, song).await?;
            }
        } else {
            tracing::info!("Bad source");
        }
    } else {
        args.restore();
        search(ctx, msg, args).await?;
    }

    Ok(())
}

#[command]
#[description("Skips the current song that's playing")]
pub async fn skip(ctx: &Context, msg: &Message) -> CommandResult {
    tracing::info!("Skipping current song");
    let songbird = get_voice(ctx).await;

    if let Some(call) = msg.guild_id.and_then(|guild| songbird.get(guild)) {
        let manager = call.lock().await;
        manager.queue().skip()?;
    } else {
        tracing::info!("Failed to get call for guild {:?}", msg.guild_id);
        Err(Error::internal("Failed to get guild"))?;
    }

    Ok(())
}

#[command]
#[description("Stop the current playlist")]
pub async fn stop(ctx: &Context, msg: &Message) -> CommandResult {
    tracing::info!("Stopping playing");
    let songbird = get_voice(ctx).await;
    if let Some(call) = msg.guild_id.and_then(|guild| songbird.get(guild)) {
        let manager = call.lock().await;
        manager.queue().stop();
        Ok(())
    } else {
        tracing::info!("Failed to get call for guild {:?}", msg.guild_id);
        Err(Error::internal("Failed to get guild"))?;
        Ok(())
    }
}

#[command]
#[only_in(guild)]
pub async fn search(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let query = args.rest();
    tracing::info!("Searching for {}", query);
    if let Some(id) = youtube::search(query).await? {
        let song_source = songbird::ytdl(&youtube::id_to_link(&id))
            .await
            .map_err(|e| Error::internal(format!("Failed to load video for {} {:?}", id, e)))?;

        join_channel_of_author(ctx, msg).await?;
        let queue_pos = queue_song(ctx, &msg.guild_id.unwrap(), song_source).await?;
        write_queue_response(ctx, msg, queue_pos, &id).await?;

        let song = Song::from_ctx(ctx, msg, &id).await?;

        if let Some(db) = ctx.data.read().await.get::<database::DbConnectionKey>() {
            database::record_song_request(db, song).await?;
        }
    } else {
        discord::message_channel(
            ctx,
            &msg.channel_id,
            format!("Couldn't find anything for that search"),
        )
        .await?;
    }

    Ok(())
}

enum QueueRemovalOp {
    First,
    Last,
    Index(usize),
}

impl fmt::Display for QueueRemovalOp {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            QueueRemovalOp::First => write!(f, "first"),
            QueueRemovalOp::Last => write!(f, "last"),
            QueueRemovalOp::Index(index) => write!(f, "#{}", index),
        }
    }
}

impl TryFrom<Option<String>> for QueueRemovalOp {
    type Error = String;

    fn try_from(value: Option<String>) -> Result<Self, Self::Error> {
        match value.as_ref().map(|s| s.as_str()) {
            None | Some("last") => Ok(QueueRemovalOp::Last),
            Some("first") => Ok(QueueRemovalOp::First),
            Some(value) => {
                if let Ok(index) = value.parse::<usize>() {
                    Ok(QueueRemovalOp::Index(index))
                } else {
                    Err(format!("{} wasn't a number", value))
                }
            }
        }
    }
}

#[command]
#[only_in(guild)]
#[description = "Modify the queue queue list|remove|clear"]
pub async fn queue(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let arg = args.single::<String>().ok();
    tracing::info!("Queue arg: {:?}", arg);

    match arg.as_deref() {
        None | Some("list") => print_queue(ctx, msg).await?,
        Some("remove") => remove_from_queue(ctx, msg, args).await?,
        Some("clear") => clear_queue(ctx, msg).await?,
        Some(other) => {
            discord::message_channel(
                ctx,
                &msg.channel_id,
                format!("{} wasn't a recognised command", other),
            )
            .await?;
        }
    }

    Ok(())
}

mod queue {
    use super::*;

    pub async fn clear_queue(ctx: &Context, msg: &Message) -> CommandResult {
        let manager = discord::get_voice(ctx).await;
        if let Some(call) = manager.get(msg.guild_id.unwrap()) {
            let call = call.lock().await;
            let queue = call.queue();

            queue.stop();
            queue.modify_queue(|q| while let Some(_) = q.pop_front() {});
        } else {
            discord::message_channel(ctx, &msg.channel_id, "No Queue created").await?;
        }

        Ok(())
    }

    pub async fn remove_from_queue(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
        let what = match QueueRemovalOp::try_from(args.single::<String>().ok()) {
            Err(e) => {
                discord::message_channel(ctx, &msg.channel_id, e).await?;
                return Ok(());
            }
            Ok(op) => op,
        };

        let manager = discord::get_voice(ctx).await;
        if let Some(call) = manager.get(msg.guild_id.unwrap()) {
            tracing::info!("Getting call lock");
            let call = call.lock().await;
            tracing::info!("Got lock");
            let queue = call.queue();

            queue.modify_queue(|q| match what {
                QueueRemovalOp::First => q.pop_front(),
                QueueRemovalOp::Last => q.pop_back(),
                QueueRemovalOp::Index(index) => q.remove(index),
            });

            discord::message_channel(ctx, &msg.channel_id, format!("Removed {} from queue", what))
                .await?;
        } else {
            discord::message_channel(ctx, &msg.channel_id, "No Queue created").await?;
        }

        Ok(())
    }

    pub async fn print_queue(ctx: &Context, msg: &Message) -> CommandResult {
        let manager = discord::get_voice(ctx).await;
        if let Some(call) = manager.get(msg.guild_id.unwrap()) {
            tracing::info!("Getting call lock");
            let call = call.lock().await;
            tracing::info!("Got lock");
            let queue = call.queue();

            if queue.is_empty() {
                discord::message_channel(ctx, &msg.channel_id, "Queue is empty").await?;
                return Ok(());
            }

            let queue = queue
                .current_queue()
                .into_iter()
                .enumerate()
                .map(|(pos, track)| {
                    (
                        format_queue_pos(pos),
                        util::title_author_from_metadata(track.metadata()),
                    )
                })
                .collect::<Vec<_>>();
            discord::send_embedded_list(ctx, &msg.channel_id, queue, false).await?;
        } else {
            discord::message_channel(ctx, &msg.channel_id, "No Queue created").await?;
        }

        Ok(())
    }
}

fn format_queue_pos(pos: usize) -> String {
    match pos {
        0 => String::from("Current"),
        pos => pos.to_string(),
    }
}

/// Add a song to the internal queue
async fn queue_song(ctx: &Context, guild_id: &GuildId, source: Input) -> Result<usize, Error> {
    tracing::info!("Starting play");
    let manager = discord::get_voice(ctx).await;

    if let Some(handler_lock) = manager.get(*guild_id) {
        let mut handler = handler_lock.lock().await;
        handler.enqueue_source(source);
        tracing::info!("Queued song, currently {} in queue", handler.queue().len());

        Ok(handler.queue().len())
    } else {
        Err(Error::internal("Failed to get call for guild"))
    }
}

/// Play a song now
pub async fn play_now(
    ctx: &Context,
    guild_id: &GuildId,
    source: Input,
    vol: f32,
) -> Result<(), Error> {
    tracing::info!("Starting play");
    let manager = discord::get_voice(ctx).await;

    if let Some(handler_lock) = manager.get(*guild_id) {
        tracing::info!("Waiting on lock in play");
        let mut handler = handler_lock.lock().await;
        tracing::info!("Got lock in play");
        let track_handle = handler.play_source(source);
        track_handle.set_volume(vol)?;
    }

    tracing::info!("Done playing");
    Ok(())
}
