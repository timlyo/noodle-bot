use crate::discord;
use crate::error::Error;
use async_trait::async_trait;
use serenity::{framework::standard::Args, model::prelude::Message, prelude::Context};
use structopt::StructOpt;

pub fn parse_args<T>(args: Args) -> Result<T, structopt::clap::Error>
where
    T: StructOpt,
{
    // Dummy is required because structopt throws away the first argument
    let dummy = Vec::from([String::from("")]).into_iter();
    let args = args.raw_quoted().map(|a| a.to_string());

    T::from_iter_safe(dummy.chain(args))
}

#[async_trait]
pub trait CommunicableError {
    async fn communicate_error(self, ctx: &Context, msg: &Message) -> Self
    where
        Self: Sized + Send;
}

#[async_trait]
impl<I: Send> CommunicableError for Result<I, Error> {
    async fn communicate_error(self, ctx: &Context, msg: &Message) -> Self
    where
        Self: Sized + Send,
    {
        let user_msg = match self.as_ref() {
            Ok(_) => None,
            Err(Error::UserFacing(error)) => Some(error.clone()),
            Err(e) => Some(format!("Unexpected error: {:?} <@{}>", e, crate::TIMLYO)),
        };

        if let Some(user_msg) = user_msg {
            if let Err(e) = discord::message_channel(ctx, &msg.channel_id, user_msg).await {
                tracing::error!("Failed to send error to client {:?}", e);
            }
        }

        self
    }
}
