pub mod commands;
pub mod database;
pub mod discord;
pub mod error;
pub mod events;
pub mod util;
pub mod youtube;

use cached::SizedCache;
use serenity::{
    async_trait,
    client::{Client, Context, EventHandler},
    framework::standard::CommandError,
    framework::standard::{
        help_commands,
        macros::{group, help, hook},
        Args, StandardFramework,
    },
    framework::standard::{CommandGroup, CommandResult, HelpOptions},
    model::channel::Message,
    model::prelude::UserId,
    prelude::RwLock,
};
use songbird::{
    input::Input,
    typemap::{TypeMap, TypeMapKey},
    SerenityInit,
};
use std::{collections::HashSet, env, sync::Arc};

use error::Error;

use commands::*;
use tracing_subscriber::prelude::*;

const TIMLYO: u64 = 147017237100888064;
const TEKKERS: u64 = 303530536423522314;
const DEXTER: u64 = 130083105582088193;
const TIMBOB: u64 = 213605628676014080;

#[group]
#[prefixes("d", "dexter", "n", "noodles")]
#[commands(roulette, sing, noodles, skip, stop, stats, search, time, queue)]
#[sub_groups(ROLLTABLE)]
struct Dexter;

struct Handler;

#[async_trait]
impl EventHandler for Handler {}

#[hook]
async fn before(_: &Context, _msg: &Message, _cmd: &str) -> bool {
    tracing::info!("### Starting Command ###");
    true
}

#[hook]
async fn log_errors(_: &Context, _msg: &Message, cmd: &str, error: Result<(), CommandError>) {
    if error.is_err() {
        tracing::error!("{}, {:?}", cmd, error);
    }
    tracing::info!("### Finished Command ###");
}

#[help]
async fn help(
    ctx: &Context,
    msg: &Message,
    args: Args,
    help_options: &'static HelpOptions,
    groups: &[&'static CommandGroup],
    owners: HashSet<UserId>,
) -> CommandResult {
    let _ = help_commands::with_embeds(ctx, msg, args, help_options, groups, owners).await;
    Ok(())
}

#[hook]
async fn unrecognised_command_hook(ctx: &Context, msg: &Message, unrecognised_command_name: &str) {
    let _ = discord::message_channel(
        ctx,
        &msg.channel_id,
        format!(
            "Didn't recognise {:?} as a command",
            unrecognised_command_name
        ),
    )
    .await;
}

pub async fn run() {
    let _ = dotenv::dotenv();
    let _guard = sentry::init((
        env::var("SENTRY_KEY").expect("No sentry key"),
        sentry::ClientOptions {
            release: sentry::release_name!(),
            ..Default::default()
        },
    ));

    tracing_subscriber::fmt().compact().finish().init();

    let framework = StandardFramework::new()
        .configure(|c| c.prefix("!").allow_dm(false).ignore_bots(false))
        .before(before)
        .after(log_errors)
        .unrecognised_command(unrecognised_command_hook)
        .help(&HELP)
        .group(&DEXTER_GROUP);

    let token = env::var("DISCORD_TOKEN").expect("token");
    let mut client = Client::builder(token)
        .event_handler(Handler)
        .framework(framework)
        .register_songbird()
        .await
        .expect("Error creating client");

    stash_statsd_connection(&client.data).await;
    stash_db_connection_pool(&client.data).await;
    stash_id_cache(&client.data).await;

    tracing::info!("Starting");
    if let Err(why) = client.start().await {
        tracing::info!("An error occurred while running the client: {:?}", why);
    }
}

pub struct StatsdKey {}

impl TypeMapKey for StatsdKey {
    type Value = statsd::Client;
}

pub async fn stash_statsd_connection(data: &Arc<RwLock<TypeMap>>) {
    let client =
        statsd::Client::new("127.0.0.1:8086", "noodles").expect("Statsd connection failed");

    tracing::info!("Connecting to statsd");

    client.incr("starts");

    let mut data = data.write().await;
    data.insert::<StatsdKey>(client);
}

pub async fn stash_db_connection_pool(data: &Arc<RwLock<TypeMap>>) {
    let db = match database::get_connection().await {
        Ok(db) => db,
        Err(err) => {
            return tracing::info!("Failed to get DB connection: {:?}", err);
        }
    };

    let mut data = data.write().await;
    data.insert::<database::DbConnectionKey>(db);
}

pub async fn stash_id_cache(data: &Arc<RwLock<TypeMap>>) {
    let mut data = data.write().await;
    data.insert::<IdCacheKey>(cached::SizedCache::with_size(256));
}

pub struct IdCacheKey {}

impl TypeMapKey for IdCacheKey {
    type Value = SizedCache<String, youtube::Snippet>;
}

pub async fn get_noodles() -> Result<Input, songbird::input::error::Error> {
    songbird::ffmpeg("./Noodles.wav").await
}
