use rand::Rng;
use serenity::model::id::{GuildId, UserId};
use sqlx::PgPool;
use std::collections::HashSet;
use tokio;

use discord_bot::commands::roll_table::database;

async fn get_db() -> PgPool {
    discord_bot::database::get_connection()
        .await
        .expect("Couldn't connect to db")
}

macro_rules! test_name {
    () => {{
        fn f() {}
        fn type_name_of<T>(_: T) -> &'static str {
            std::any::type_name::<T>()
        }
        let name = type_name_of(f);
        &name[..name.len() - 16]
    }};
}

#[tokio::test]
async fn creating_tables_and_adding_items() {
    let db = get_db().await;

    let table = test_name!();
    let guild = GuildId(rand::thread_rng().gen());
    let creator = UserId(rand::thread_rng().gen());

    database::insert_new_table(&db, guild, creator, table, None, false)
        .await
        .unwrap();

    let table_info = database::get_table_by_name(&db, table, None, guild)
        .await
        .unwrap()
        .unwrap();

    for item in 0..10 {
        database::insert_new_item(&db, guild, &item.to_string(), table, None, 1)
            .await
            .unwrap();
    }

    let items = database::get_table_items(&db, table_info.id).await.unwrap();
    let ids: HashSet<_> = items.into_iter().map(|i| i.content).collect();
    let expected: HashSet<_> = (0..10).map(|i| i.to_string()).collect();

    assert_eq!(ids, expected);
}

#[tokio::test]
async fn other_guilds_cannot_access_roll_tables() {
    let db = get_db().await;

    let table = test_name!();
    let guild = GuildId(rand::thread_rng().gen());
    let creator = UserId(rand::thread_rng().gen());

    database::insert_new_table(&db, guild, creator, table, None, false)
        .await
        .unwrap();

    let other_guild = GuildId(rand::thread_rng().gen());

    let res = database::get_table_by_name(&db, table, None, other_guild)
        .await
        .unwrap();

    assert!(res.is_none());
}

#[tokio::test]
async fn delete_only_deletes_a_guilds_roll_table() {
    let db = get_db().await;

    let table = test_name!();
    let guild1 = GuildId(rand::thread_rng().gen());
    let guild2 = GuildId(rand::thread_rng().gen());
    let creator = UserId(rand::thread_rng().gen());

    let table1_id = database::insert_new_table(&db, guild1, creator, table, None, false)
        .await
        .unwrap();
    let table2_id = database::insert_new_table(&db, guild2, creator, table, None, false)
        .await
        .unwrap();

    database::flag_table_deleted(&db, table1_id).await.unwrap();

    let table1 = database::get_table_by_name(&db, table, None, guild1)
        .await
        .unwrap()
        .unwrap();
    let table2 = database::get_table_by_name(&db, table, None, guild2)
        .await
        .unwrap()
        .unwrap();

    assert_eq!(table1_id, table1.id);
    assert_eq!(table2_id, table2.id);

    assert_eq!(table1.deleted, true);
    assert_eq!(table2.deleted, false);
}

#[tokio::test]
async fn deleting_and_recreating_a_table_clears_it() {
    let db = get_db().await;

    let table = test_name!();
    let guild = GuildId(rand::thread_rng().gen());
    let creator = UserId(rand::thread_rng().gen());

    let table_id = database::insert_new_table(&db, guild, creator, table, None, false)
        .await
        .unwrap();

    for x in 0..5 {
        database::insert_new_item(&db, guild, &x.to_string(), table, None, 1)
            .await
            .unwrap();
    }

    assert_eq!(
        database::get_table_items(&db, table_id)
            .await
            .unwrap()
            .len(),
        5
    );

    database::flag_table_deleted(&db, table_id).await.unwrap();

    let new_table_id = database::insert_new_table(&db, guild, creator, table, None, false)
        .await
        .unwrap();

    assert_ne!(table_id, new_table_id);

    assert!(database::get_table_items(&db, table_id)
        .await
        .unwrap()
        .is_empty());
}

#[tokio::test]
async fn roll_tables_are_unique_per_guild_and_group() {
    let db = get_db().await;

    let table = test_name!();
    let group = "test_group";
    let guild = GuildId(rand::thread_rng().gen());
    let creator = UserId(rand::thread_rng().gen());

    database::insert_new_table(&db, guild, creator, table, None, false)
        .await
        .unwrap();

    assert!(
        database::insert_new_table(&db, guild, creator, table, None, false)
            .await
            .is_err()
    );

    database::insert_new_table(&db, guild, creator, table, Some(group), false)
        .await
        .unwrap();

    assert!(
        database::insert_new_table(&db, guild, creator, table, Some(group), false)
            .await
            .is_err()
    );
}

#[tokio::test]
async fn deleted_tables_arent_returned_by_list_tables() {
    let db = get_db().await;

    let table = test_name!();
    let guild = GuildId(rand::thread_rng().gen());
    let creator = UserId(rand::thread_rng().gen());

    let table_id = database::insert_new_table(&db, guild, creator, table, None, false)
        .await
        .unwrap();
    database::flag_table_deleted(&db, table_id).await.unwrap();

    let tables = database::get_tables_for_guild(&db, guild).await.unwrap();

    assert!(tables.is_empty());

    database::restore_table(&db, table_id).await.unwrap();

    let tables = database::get_tables_for_guild(&db, guild).await.unwrap();

    assert_eq!(tables.len(), 1);
    assert_eq!(tables[0].id, table_id);
}
