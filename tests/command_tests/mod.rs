//! Tests that send data to discord

use serenity::framework::StandardFramework;
use serenity::http::Http;
use serenity::model::id::{ChannelId, GuildId};
use serenity::model::prelude::Message;
use serenity::Client;
use sqlx::PgPool;
use std::env;
use tokio::time::Duration;

pub mod get_stats;
pub mod roll_table;

const CHANNEL_ID: u64 = 818194662333481030;

async fn get_messages_after(msg: &Message, ctx: &Http, count: usize) -> Vec<Message> {
    let mut res = None;
    for _ in 0..30 {
        let mut messages = msg
            .channel_id
            .messages(ctx, |g| g.after(msg.id))
            .await
            .unwrap();

        if messages.len() >= count {
            messages.reverse();
            return messages;
        } else {
            tokio::time::sleep(Duration::from_millis(300)).await
        }

        res = Some(messages);
    }

    panic!("Failed to get expected messages. Found {:?}", res);
}

pub async fn get_framework() -> (Client, ChannelId, GuildId, PgPool) {
    let _ = dotenv::dotenv();
    let token = env::var("DISCORD_TEST_TOKEN").expect("token");
    let framework = StandardFramework::new();
    let client = Client::builder(token)
        .framework(framework)
        .await
        .expect("Error creating client");

    let channel = ChannelId(CHANNEL_ID);

    let guild = channel
        .to_channel(&client.cache_and_http)
        .await
        .unwrap()
        .guild()
        .unwrap()
        .guild_id;

    let database = discord_bot::database::get_connection().await.unwrap();

    (client, channel, guild, database)
}
