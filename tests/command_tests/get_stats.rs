use crate::command_tests::{get_framework, get_messages_after};

#[tokio::test]
async fn get_stats() {
    let (client, channel, guild, database) = get_framework().await;

    let message =
        discord_bot::discord::message_channel(&client.cache_and_http.http, &channel, "!d stats")
            .await
            .unwrap();

    let messages = get_messages_after(&message, &*client.cache_and_http.http, 2).await;

    let songs = discord_bot::database::count_songs(&database, &guild)
        .await
        .unwrap();

    assert_eq!(
        messages[0].content,
        format!("Played {} total songs, {} unique", songs.1, songs.0)
    );
}
