use crate::command_tests;
use discord_bot::discord;
use futures::FutureExt;
use serenity::http::CacheHttp;

#[tokio::test]
async fn create_and_roll_against_table_then_delete() {
    let (client, channel, _guild, _db) = command_tests::get_framework().await;

    let table_id = uuid::Uuid::new_v4().to_string().replace('-', "");

    let send_message = |msg| {
        discord::message_channel(&client.cache_and_http.http, &channel, msg).map(|r| r.unwrap())
    };

    // Create table
    {
        let msg = send_message(format!("!noodles rolltable new {}", table_id)).await;
        let replies =
            command_tests::get_messages_after(&msg, &client.cache_and_http.http(), 1).await;
        assert_eq!(
            replies[0].content,
            format!("Created a new table called {}", table_id)
        );
    }

    // List
    {
        let msg = send_message(String::from("!noodles rt list")).await;
        let replies =
            command_tests::get_messages_after(&msg, &client.cache_and_http.http(), 1).await;
        assert!(replies[0].embeds[0]
            .description
            .as_deref()
            .unwrap()
            .contains(&table_id.to_string()));
    }

    // List Items
    {
        let msg = send_message(format!("!noodles rt list {}", table_id)).await;
        let replies =
            command_tests::get_messages_after(&msg, &client.cache_and_http.http(), 1).await;
        assert!(replies[0]
            .content
            .starts_with(&format!("{} doesn't have any items yet", table_id)))
    }

    // Add items to it
    for x in 0..5 {
        let msg = send_message(format!("!n rt add {} {}", table_id, x)).await;

        let reply = command_tests::get_messages_after(&msg, &client.cache_and_http.http(), 1).await;

        assert_eq!(
            reply[0].content,
            format!("Added to {}, it now has {} items", table_id, x + 1)
        );
    }

    // List Items
    {
        let msg = send_message(format!("!noodles rt list {}", table_id)).await;
        let replies =
            command_tests::get_messages_after(&msg, &client.cache_and_http.http(), 1).await;
        assert_eq!(
            replies[0].embeds[0].description,
            Some(format!("0, 1, 2, 3, 4"))
        );
    }

    // Roll
    {
        for _ in 0..5usize {
            let msg = send_message(format!("!noodles rolltable roll {}", table_id)).await;
            let reply =
                command_tests::get_messages_after(&msg, &client.cache_and_http.http(), 1).await;

            assert!(reply[0].content.starts_with("I choose"));
        }
    }

    // Remove
    {
        let msg = send_message(format!("!noodles rolltable remove {} 3", table_id)).await;
        let reply = command_tests::get_messages_after(&msg, &client.cache_and_http.http(), 1).await;
        assert_eq!(
            reply[0].content,
            format!(
                "Removed 3 from {}. Run `!n rt add {} 3` to restore it",
                table_id, table_id
            )
        );

        let msg = send_message(format!("!n rt list table {}", table_id)).await;
        let reply = command_tests::get_messages_after(&msg, &client.cache_and_http.http(), 1).await;

        assert!(!reply[0].content.contains("3"));
    }

    // Delete
    {
        let msg = send_message(format!("!n rt remove {}", table_id)).await;
        let reply = command_tests::get_messages_after(&msg, &client.cache_and_http.http(), 1).await;

        assert_eq!(reply[0].content, format!("Removed {tid}. Use `!n rt restore {tid}` to get it back or `!n rt new {tid}` to overwrite it forever", tid=table_id));

        let list_msg = send_message(String::from("!n rt list")).await;
        let reply =
            command_tests::get_messages_after(&list_msg, &client.cache_and_http.http(), 1).await;
        assert!(!reply[0].content.contains(&table_id));
    }

    // Restore
    {
        let msg = send_message(format!("!n rt restore {}", table_id)).await;
        let reply = command_tests::get_messages_after(&msg, &client.cache_and_http.http(), 1).await;
        assert_eq!(reply[0].content, format!("Restored {}", table_id));

        // Check still in list of tables
        let list_msg = send_message(String::from("!n rt list")).await;
        let reply =
            command_tests::get_messages_after(&list_msg, &client.cache_and_http.http(), 1).await;
        assert!(reply[0].embeds[0]
            .description
            .as_ref()
            .unwrap()
            .contains(&table_id));

        // Check it still has it's items
        let msg = send_message(format!("!n rt list {}", table_id)).await;
        let reply = command_tests::get_messages_after(&msg, &client.cache_and_http.http(), 1).await;
        assert_eq!(
            reply[0].embeds[0].description.as_ref().unwrap(),
            "0, 1, 2, 4"
        );
    }

    // Delete and Overwrite
    {
        let msg = send_message(format!("!n rt remove {}", table_id)).await;
        let reply = command_tests::get_messages_after(&msg, &client.cache_and_http.http(), 1).await;
        assert_eq!(reply[0].content, format!("Removed {tid}. Use `!n rt restore {tid}` to get it back or `!n rt new {tid}` to overwrite it forever", tid=table_id));

        let msg = send_message(format!("!n rt new {}", table_id)).await;
        let reply = command_tests::get_messages_after(&msg, &client.cache_and_http.http(), 1).await;
        assert_eq!(
            reply[0].content,
            format!("Created a new table called {}", table_id)
        );
    }

    // Final delete
    {
        let msg = send_message(format!("!n rt remove {}", table_id)).await;
        let reply = command_tests::get_messages_after(&msg, &client.cache_and_http.http(), 1).await;
        assert_eq!(reply[0].content, format!("Removed {tid}. Use `!n rt restore {tid}` to get it back or `!n rt new {tid}` to overwrite it forever", tid=table_id));
    }
}
