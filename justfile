start_db:
    docker run --name postgres -e POSTGRES_PASSWORD=password -e POSTGRES_USER=postgres -ePOSTGRES_DB=robo_dexter -p 5432:5432 -d postgres

start_grafana:
    docker run -d -p 3000:3000 grafana/grafana

build:
    cargo build --release

package: build
    makepkg -f

# Deployment stuff

deploy host: package
    @echo "Deploying to {{host}}"
    rsync -v discord_bot*.pkg.tar.xz {{host}}:/tmp/

    ssh {{host}} -t sudo pacman -U /tmp/discord_bot*.pkg.tar.xz

migrate_db host:
    ssh {{host}} -t cd /opt/discord_bot && sqlx migrate run


# All the links I forget

open_sentry:
    firefox https://sentry.io

open_discord_dev:
    firefox https://discord.com/developers/applications

