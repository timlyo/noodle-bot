CREATE TABLE songs (
    requester VARCHAR NOT NULL,
    guild VARCHAR NOT NULL,
    channel VARCHAR NOT NULL,
    yt_video VARCHAR NOT NULL,
    time_requested TIMESTAMPTZ NOT NULL
)