ALTER TABLE songs
    ADD COLUMN audio_channel VARCHAR;

ALTER TABLE songs
    RENAME channel TO request_channel;