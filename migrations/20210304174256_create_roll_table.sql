CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "fuzzystrmatch";

CREATE TABLE roll_tables(
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    guild_id VARCHAR NOT NULL,
    creator_id VARCHAR NOT NULL,
    name VARCHAR(256) NOT NULL,
    table_group VARCHAR(256),
    global_table BOOLEAN DEFAULT FALSE NOT NULL,
    weighted BOOLEAN NOT NULL DEFAULT FALSE,
    deleted BOOLEAN NOT NULL DEFAULT FALSE
);

-- Ensure that rolltables are unique per guild and group
CREATE UNIQUE INDEX roll_tables_unique ON roll_tables
    (guild_id, name, COALESCE(table_group, ''));

CREATE TABLE roll_table_items(
    content VARCHAR NOT NULL,
    roll_table UUID REFERENCES roll_tables (id) ON DELETE CASCADE NOT NULL,
    link VARCHAR,
    weight SMALLINT DEFAULT 1 NOT NULL CHECK (weight > 0 AND weight < 100),

    UNIQUE(roll_table, content)
);
